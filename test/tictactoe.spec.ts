import {assert, expect} from "chai";

type O = "o";
type X = "x";

type Player = O | X;

type NW = "a1";
type W = "a2";
type SW = "a3";
type N = "b1";
type C = "b2";
type S = "b3";
type NE = "c1";
type E = "c2";
type SE = "c3";

type Position = NW | N | NE | W | C | E | SW | S | SE;

type Board = Map<Position, Player>;

const positions: Set<Position> = new Set(["a1", "a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3"]);

type Next<CURRENT extends Player, AVAILABLE extends Position, TARGET extends AVAILABLE> =
    Move<Exclude<Player, CURRENT>, Exclude<AVAILABLE, TARGET>> | Won<CURRENT>;

class Move<CURRENT extends Player, AVAILABLE extends Position> {

    public readonly kind: "move" = "move";

    private readonly board: Board;
    private readonly current: CURRENT;

    constructor(board: Board, current: CURRENT) {
        this.board = board;
        this.current = current;
    }

    public move<TARGET extends AVAILABLE>(player: CURRENT, pos: TARGET): Next<CURRENT, AVAILABLE, TARGET> {
        this.board.set(pos, player);
        if (this.hasWon(player)) {
            return new Won(player);
        }
        return new Move(this.board, this.nextPlayer());
    }

    public whoseTurn(): CURRENT {
        return this.current;
    }

    public availableMoves(): Set<AVAILABLE> {
        const availablePositions = new Set(positions);
        for (const occupied of this.board.keys()) {
            availablePositions.delete(occupied);
        }
        return availablePositions as Set<AVAILABLE>;
    }

    // TODO other functions like isPositionOccupied

    public hasEnded() {
        return this.availableMoves().size === 0;
    }

    private hasWon(player: CURRENT) {
        return this.board.get("a1") === player &&
         this.board.get("a2") === player &&
         this.board.get("a3") === player;
    }

    private nextPlayer(): Exclude<Player, CURRENT> {
        return (this.current === "x" ? "o" : "x") as Exclude<Player, CURRENT>;
    }

}

class Won<WINNER extends Player>  {

    public readonly kind: "won" = "won";

    private readonly theWinner: WINNER;

    constructor(current: WINNER) {
        this.theWinner = current;
    }

    public winner(): WINNER {
        return this.theWinner;
    }

    public hasEnded() {
        return true;
    }
}

class TicTacToe extends Move<X, Position> {
    constructor() {
        super(new Map(), "x");
    }
}

describe("tic tac toe", () => {

    let ticTacToe: TicTacToe;

    beforeEach(() => {
        ticTacToe = new TicTacToe();
    });

    it("does not play out of turn", () => {
        const move1 = ticTacToe.move("x", "b1");
        switch (move1.kind) {
            case "move":
                expect(move1.whoseTurn()).to.equal("o");
                break;
            case "won":
                assert.fail("no winner yet");
        }
    });

    it("does not play field twice", () => {
        const firstMove = ticTacToe.move("x", "a1");
        switch (firstMove.kind) {
            case "move":
                // firstMove.move("o", "a1"); // does not compile
                expect(firstMove.availableMoves()).to.
                    deep.equal(new Set(["a2", "a3", "b1", "b2", "b3", "c1", "c2", "c3"]));
                break;
            case "won":
                assert.fail("no winner yet");
        }
    });

    // it("10th move not possible", () => {
    //     const tieGame = new TicTacToe()
    //         .move("x", "a1")
    //         .move("o", "a2")
    //         .move("x", "a3")
    //         .move("o", "b1")
    //         .move("x", "b2")
    //         .move("o", "b3")
    //         .move("x", "c1")
    //         .move("o", "c2")
    //         .move("x", "c3");
    //     // .move("o", "a2")
    //     expect(tieGame.hasEnded()).is.equal(true);
    // });

    it("won game has winner", () => {
        const move1 = ticTacToe.move("x", "a1");
        switch (move1.kind) {
            case "move":
                const move2 = move1.move("o", "b1");
                switch (move2.kind) {
                    case "move":
                        const move3 = move2.move("x", "a2");
                        switch (move3.kind) {
                            case "move":
                                const move4 = move3.move("o", "b2");
                                switch (move4.kind) {
                                    case "move":
                                        const wonGame = move4.move("x", "a3");
                                        switch (wonGame.kind) {
                                            case "move":
                                                assert.fail("winner yet");
                                                break;
                                            case "won":
                                                expect(wonGame.hasEnded()).is.equal(true);
                                                expect(wonGame.winner()).is.equal("x");
                                        }
                                        break;
                                    case "won":
                                        assert.fail("no winner yet");
                                }
                                break;
                            case "won":
                                assert.fail("no winner yet");
                        }
                        break;
                    case "won":
                        assert.fail("no winner yet");
                }
                break;
            case "won":
                assert.fail("no winner yet");
        }
    });
});
